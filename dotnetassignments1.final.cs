using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.IO;

namespace Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            var KeepAlive = true;

            while (KeepAlive)
            {
                try
                {
                    Console.Write("Enter assignment number (or -1 to exit): ");
                    var assigmentChoice = int.Parse(Console.ReadLine() ?? "");
                    Console.ForegroundColor = ConsoleColor.Green;

                    switch (assigmentChoice)
                    {
                        case 1:
                            RunExerciseOne();
                            break;
                        case 2:
                            RunExerciseTwo();
                            break;
                        case 3:
                            RunExerciseThree();
                            break;
                        case 4:
                            RunExerciseFour();
                            break;
                        case 5:
                            RunExerciseFive();
                            break;
                        case 6:
                            RunExcerciseSix();
                            break;
                        case 7:
                            RunExerciseSeven();
                            break;
                        case 8:
                            RunExcerciseEight();
                            break;
                        case 9:
                            RunExerciseNine();
                            break;
                        case 10:
                            RunExerciseTen();
                            break;
                        case 11:
                            RunExerciseEleven();
                            break;
                        case 12:
                            RunExerciseTwelve();
                            break;
                        case 13:
                            RunExerciseThirteen();
                            break;
                        case 14:
                            RunExerciseFourteen();
                            break;
                        case 15:
                            RunExerciseFifteen();
                            break;
                        case 16:
                            RunExerciseSixteen();
                            break;
                        case 17:
                            RunExerciseSeventeen();
                            break;
                        case 18:
                            RunExerciseEighteen();
                            break;
                        case 19:
                            RunExerciseNineteen();
                            break;
                        case 20:
                            RunExerciseTwenty();
                            break;
                        case 21:
                            RunExerciseTwentyOne();
                            break;
                        case 22:
                            RunExerciseTwentyTwo();
                            break;
                        case 23:
                            RunExerciseTwentyThree();
                            break;
                        case 24:
                            RunExerciseTwentyFour();
                            break;
                        case 25:
                            RunExerciseTwentyFive();
                            break;
                        case 26:
                            RunExerciseTwentySix();
                            break;
                        case 27:
                            RunExerciseTwentySeven();
                            break;
                        case 28:
                            RunExerciseTwentyEight();
                            break;
                        case -1:
                            KeepAlive = false;
                            break;
                        default:
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("This is not a valid assigment number!");
                            break;
                    }

                    Console.ResetColor();
                    Console.WriteLine("Hit any key to continue!");
                    Console.ReadKey();
                    Console.Clear();
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("This is not a valid assigment number!");
                    Console.ResetColor();
                }
            }
        }

        private static void RunExerciseOne()
        {
            string firstName; //Variable for storing string value
            string lastName; // --- | | ---
            //Displaying message for entring value
            Console.WriteLine("Please tell us your first name");

            //Accepting and holding values in name variable
            firstName = Console.ReadLine();

            //Displaying Output
            Console.WriteLine("Please tell us your surname");

            //Holding console screen
            lastName = Console.ReadLine();

            //Final output
            Console.WriteLine("This was a triumph " + firstName + " " + lastName + "!");
            Console.WriteLine("I'm making a note here, huge success");
        }

        private static void RunExerciseTwo()
        {
            {
                //Using DateTime classes to set things straight
               DateTime today = DateTime.Now;
                // Add one day with AddDays to get tomorrow..
               DateTime tomorrow = today.AddDays(+1);
                // Subtract one day to get yesterday..
               DateTime yesterday =  DateTime.Today.AddDays(-1);
                // Some WriteLine nincompoopery and presto:
                Console.WriteLine("Todays date is: {0:dddd}", today);
                Console.WriteLine("Tomorrows date is: {0:dddd}", tomorrow);
                Console.WriteLine("Yesterdays date was: {0:dddd}", yesterday);
            }
        }
        private static void RunExerciseThree()
        {
            string firstName; //Variable for storing string value
            string lastName; // --- | | ---
            //Displaying message for entring value
            Console.WriteLine("Please tell us your first name");

            //Accepting and holding values in name variable
            firstName = Console.ReadLine();

            //Displaying Output
            Console.WriteLine("Please tell us your surname");

            //Holding console screen
            lastName = Console.ReadLine();

            //Final output
            Console.WriteLine(firstName + " " + lastName);
        }
        private static void RunExerciseFour()
        {
            //Inside the method body, create a new empty string variable below the variable str
            //Use string manipulation methods like SubString, IndexOf, Remove, Replace, Insert
            //to get the string “The brown fox jumped over the lazy dog”, where all characters match exactly and output these to the screen

            //Create string
            String str = "The quick fox Jumped Over the DOG";
            //use Replace to Replace Quick with Brown.
            str = str.Replace("quick", "brown");
            str = str.Insert(29, " lazy");
            Console.WriteLine(str.ToLower());
               //Still needs capital T in the beginning to pass :/ 
            }
        private static void RunExerciseFive()
        {
            //Declare the string
            string str = "Arrays are very common in programming, they look something like: [1,2,3,4,5]";
            //Create a substring
            string numbers = str.Substring(str.IndexOf("["));
            //Remove numbers
            numbers = numbers.Remove(numbers.IndexOf("2"), 4);
            //Add additional numbers
            numbers = numbers.Insert(numbers.IndexOf("]"), ",6,7,8,9,10");
            Console.WriteLine(numbers);
        }

        private static void RunExcerciseSix()
        {
            //Create a user input and turn it into an integer
            Console.Write("Enter a number: ");
            int a = Convert.ToInt32(Console.ReadLine());
            //Create yet another user input and turn that into an integer
            Console.Write("Enter another number: ");
            int b = Convert.ToInt32(Console.ReadLine());
            
            //With a while-loop, make sure that the user does not enter 0, for you can not divide by zero.
            while (b == 0)
            {
                Console.Write("Please enter a number that is not 0: ");
                b = Convert.ToInt32(Console.ReadLine());
            }
            //The proper output needed
            switch (a.CompareTo(b))
            {
                case 1:
                    Console.WriteLine("{0} is greater then {1} ", a, b);
                    break;
                case 0:
                    Console.WriteLine("{0} is equal to {1}", a, b);
                    break;
                case -1:
                    Console.WriteLine("{0} is greater then {1}", b, a);
                    break;
            }

            //The math required with the user input as variables treated through operators.
            Console.WriteLine(a + " + " + b + " = " + (a + b));
            Console.WriteLine(a + " - " + b + " = " + (a - b));
            Console.WriteLine(a + " * " + b + " = " + (a * b));
            Console.WriteLine(a + " / " + b + " = " + (a / b));

        }

        private static void RunExerciseSeven()
        {
            //The radius with a double primitive type for use of decimals.
            double radius;
            //Prompting the user to add data
            Console.Write("Please enter the radius: ");
            radius = Convert.ToDouble(Console.ReadLine());
            //Math objects and WriteLines
            Console.WriteLine("You entered radius " + radius);
            Console.WriteLine("This gives an area of " + (Math.PI * Math.Pow(radius, 2)));
            Console.WriteLine("You entered radius " + ((4 * Math.PI * Math.Pow(radius, 3)) / 3));

        }

        private static void RunExcerciseEight()
        {
            Console.Write("Please enter a number: ");
            Double n = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Square root of " + n + " is: " + Math.Sqrt(n));
            Console.WriteLine(n + " to the power of 2 is " + Math.Pow(n, 2));
            Console.WriteLine(n + " to the power of 10 is " + Math.Pow(n, 10));
        }

        private static void RunExerciseNine()
        {
            string name;
            DateTime birthday;
            int age;

            Console.Write("What is your name? ");
            name = Console.ReadLine();

            Console.WriteLine("Welcome " + name);
            Console.Write("What date were you born (mm-dd-yyyy)? ");
            birthday = DateTime.Parse(Console.ReadLine());

            age = ((DateTime.Now.Subtract(birthday)).Days) / 365;

            if (age >= 18)
            {
                Console.Write("Do you want to order a beer? (y/n) ");
                if (Console.ReadLine().Equals("y"))
                {
                    Console.WriteLine("A beer has been ordered");
                    return;
                }
            }

            else
            {
                Console.Write("Do you want to order a coke? (y/n) ");
                if (Console.ReadLine().Equals("y"))
                {
                    Console.WriteLine("A coke has been ordered");
                    return;
                }

            }

            Console.WriteLine("No other options are avaible");

        }

        private static void RunExerciseTen()
        {
            Console.Write("Enter 1,2 or 3 for different options: ");
            int choice = int.Parse(Console.ReadLine() ?? "");
            switch (choice)
            {
                case 1:
                    Console.Write("Enter a number: ");
                    double a = int.Parse(Console.ReadLine() ?? "");

                    Console.Write("Enter a number that is not 0: ");
                    double b = int.Parse(Console.ReadLine() ?? "");

                    while (b == 0)
                    {
                        Console.Write("Number cannot be 0. Please enter another number");
                        b = int.Parse(Console.ReadLine() ?? "");
                    }

                    Console.WriteLine("{0} divided by {1} = {2}", a, b, (a / b));

                    break;
                case 2:
                    RunExerciseFour();
                    break;
                case 3:
                    Console.WriteLine(Console.ForegroundColor.ToString());  //Test foreground color before change

                    if (Console.ForegroundColor == ConsoleColor.Green)
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                    }
                    else { Console.ForegroundColor = ConsoleColor.Green; }

                    Console.WriteLine(Console.ForegroundColor.ToString());  //Test foreground color after change
                    break;
            }
        }

        private static void RunExerciseEleven()
        {
            int num;

            Console.Write("Enter a number above 0: ");
            num = int.Parse(Console.ReadLine() ?? "");

            while (num < 0)
            {
                Console.Write("Number is not above 0. Please enter another number: ");
                num = int.Parse(Console.ReadLine() ?? "");
            }

            for (int i = 0; i <= num; i++)
            {
                DivisableByTwo(i);
            }

            Console.WriteLine("-------------------");

            for (int i = num; i >= 0; i--)
            {
                DivisableByTwo(i);
            }
        }

        private static void DivisableByTwo(int a)
        {
            if (a % 2 == 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Green;
            }
            Console.WriteLine(a);
        }

        private static void RunExerciseTwelve()
        {
            for (int i = 1; i <= 10; i++)
            {
                for (int y = 1; y <= 10; y++)
                {
                    Console.Write(i * y + "\t");
                }

                Console.WriteLine();

            }
        }

        private static void RunExerciseThirteen()
        {
            Random rnd = new Random();
            int randomNr = rnd.Next(1, 500);
            int guessNr;
            int tries = 1;

            Console.Write("Guess the number: ");
            guessNr = int.Parse(Console.ReadLine() ?? "");

            while (guessNr != randomNr)
            {
                switch (randomNr.CompareTo(guessNr))
                {
                    case 1:
                        Console.Write("Your guess was too small. Try again: ");
                        break;

                    case -1:
                        Console.Write("Your guess was too large. Try again: ");
                        break;
                }
                tries++;
                guessNr = int.Parse(Console.ReadLine() ?? "");
            }


            Console.WriteLine("You guessed the right number! It took you {0} tries.", tries);


        }

        private static void RunExerciseFourteen()
        {
            int sum = 0;
            int count = 0;
            int input = 0;

            while (input != -1)
            {
                Console.Write("Enter a number: ");
                input = int.Parse(Console.ReadLine() ?? "");
                if (input != -1)
                {
                    sum = sum + input;
                    count++;
                }
            }

            Console.WriteLine("Sum = " + sum);
            Console.WriteLine("Average = " + sum / count);

        }

        private static void RunExerciseFifteen()
        {
            int number;
            int count = 0;
            int sum;

            Console.Write("Please enter a number: ");
            number = int.Parse(Console.ReadLine() ?? "");

            for (int i = number - 1; i >= 1; i--)
            {
                if (number % i == 0)
                {
                    Console.WriteLine(i);
                }
            }

            Console.WriteLine("-----");

            for (int y = 2; count < 3; y++)
            {
                sum = 0;

                for (int i = y - 1; i >= 1; i--)
                {
                    if (y % i == 0)
                    {
                        sum = sum + i;
                    }
                }

                if (sum == y)
                {
                    Console.WriteLine(y);
                    count++;
                }

            }


        }

        private static void RunExerciseSixteen()
        {
            int number;
            int a = 0;
            int b = 1;

            Console.Write("Please enter a number: ");
            number = int.Parse(Console.ReadLine() ?? "");

            for (int i = 0; i < number; i++)
            {
                Console.Write(a + " ");

                int temp = a;
                a = b;
                b = temp + a;
            }
        }

        private static void RunExerciseSeventeen()
        {
            string text;

            Console.Write("Please enter a text to check: ");
            text = Console.ReadLine();

            int min = 0;
            int max = text.Length - 1;

            while (true)
            {
                if (min > max)
                {
                    Console.WriteLine(text + " is a palindrome");
                    return;
                }

                char a = text[min];
                char b = text[max];

                while (!char.IsLetterOrDigit(a))
                {
                    min++;
                    if (min > max)
                    {
                        Console.WriteLine(text + " is a palindrome");
                        return;
                    }
                    a = text[min];
                }

                while (!char.IsLetterOrDigit(b))
                {
                    max--;
                    if (min > max)
                    {
                        Console.WriteLine(text + " is a palindrome");
                        return;
                    }
                    b = text[max];
                }

                if (char.ToLower(a) != char.ToLower(b))
                {
                    Console.WriteLine(text + " is not a palindrome");
                    return;
                }
                min++;
                max--;
            }

        }

        private static void RunExerciseEighteen()
        {
            int[] numbers = new int[10];
            Random rnd = new Random();

            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = rnd.Next(0, 100);
            }

            foreach (int i in numbers)
            {
                Console.Write("[" + i + "] ");
            }

            Console.WriteLine("");
        }

        private static void RunExerciseNineteen()
        {
            int cost = 376;
            int pay;
            int change;

            //create and initialize the coin array
            int[] coins = new int[8];
            coins[0] = 1000;
            coins[1] = 500;
            coins[2] = 100;
            coins[3] = 50;
            coins[4] = 20;
            coins[5] = 10;
            coins[6] = 5;
            coins[7] = 1;

            Console.WriteLine("Money to pay: " + cost);
            Console.Write("Enter the sum to pay: ");
            pay = int.Parse(Console.ReadLine() ?? "");

            if (cost - pay == 0)
            {
                Console.WriteLine("You have given us exact money");
            }
            else
            {
                change = cost - pay;

                //Ensure the client has payed the full cost
                while (change > 0)
                {
                    Console.WriteLine("You still need to pay " + change + ": ");
                    pay = int.Parse(Console.ReadLine() ?? "");
                    change = change - pay;
                }

                //Change the change variable to a positive number
                change = change * -1;

                Console.WriteLine("Calculated change: " + change);

                foreach (int c in coins)
                {
                    int count = 0;

                    while (change > c)
                    {
                        count = change / c;
                        change = change % c;
                    }

                    Console.WriteLine(c + " coins: " + count);
                }
            }

        }

        private static void RunExerciseTwenty()
        {
            Random rnd = new Random();

            int[] arrayOne = new int[10];
            int[] arrayTwo = new int[10];

            int min = 0;
            int max = arrayTwo.Length - 1;

            for (int i = 0; i < arrayOne.Length; i++)
            {
                arrayOne[i] = rnd.Next(1, 100);
            }

            foreach (int n in arrayOne)
            {
                if (n % 2 == 0)
                {
                    arrayTwo[max--] = n;
                }
                else
                {
                    arrayTwo[min++] = n;
                }
            }

            foreach (int n in arrayTwo)
            {
                Console.Write("[" + n + "] ");
            }

            Console.WriteLine("");

        }

        private static void RunExerciseTwentyOne()
        {
            string str;
            double sum = 0;

            Console.Write("Enter a string of comma seperated numbers: ");
            str = Console.ReadLine();

            int[] numbers = Array.ConvertAll(str.Split(','), int.Parse);

            foreach (int n in numbers)
            {
                Console.Write("[" + n + "] ");
                sum = sum + n;
            }

            Console.WriteLine("\nMax: " + numbers.Max());
            Console.WriteLine("Min: " + numbers.Min());
            Console.WriteLine("Sum: " + sum);
            Console.WriteLine("Average: " + (sum / numbers.Length));

        }

        private static void RunExerciseTwentyTwo()
        {
            string text;

            string[] filter = new string[10];
            filter[0] = "fuck";
            filter[1] = "shit";
            filter[2] = "piss";


            Console.Write("Please enter sentence to filter: ");
            text = Console.ReadLine();

            foreach (string s in filter)
            {
                if (!String.IsNullOrEmpty(s))
                {
                    text = text.Replace(s, "-BLEEP-");
                }
            }

            Console.WriteLine("Filtered sentence: " + text);
        }

        private static void RunExerciseTwentyThree()
        {
            int[] numbers = new int[7];
            Random rnd = new Random();

            for (int i = 0; i < numbers.Length; i++)
            {
                int temp;

                do
                {
                    temp = rnd.Next(1, 40);
                } while (numbers.Contains(temp));

                numbers[i] = temp;
            }

            foreach (int i in numbers)
            {
                Console.Write("[" + i + "] ");
            }

            Console.WriteLine();
        }

        private static void RunExerciseTwentyFour()
        {
            Random rnd = new Random();
            int[] deck = { 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10, 10, 11, 11, 11, 11, 12, 12, 12, 12, 13, 13, 13, 13 };
            int[] hand = new int[5];
            int cardsInHand = 0;

            ShuffleCards(ref deck);

            for (int i = 0; i < 5; i++)
            {
                int n = rnd.Next(0, deck.Length);
                hand[cardsInHand++] = DrawCard(ref deck, n);
            }

            Console.Write("Cards drawn were: ");

            foreach (int c in hand)
            {
                Console.Write("[" + c + "] ");
            }

            Console.WriteLine();


        }

        private static int DrawCard(ref int[] deck, int n)
        {
            int temp = deck[n];

            List<int> lst = deck.ToList();
            lst.RemoveAt(n);
            deck = lst.ToArray();

            return temp;
        }

        private static void ShuffleCards(ref int[] deck)
        {
            int n = deck.Length;
            Random rnd = new Random();

            //Fisher–Yates_shuffle
            for (int i = 0; i < n; i++)
            {
                int r = i + (int)(rnd.NextDouble() * (n - i));
                int t = deck[r];
                deck[r] = deck[i];
                deck[i] = t;
            }
        }

        private static void RunExerciseTwentyFive()
        {
            int a; int b;

            a = GetValidInt();
            b = GetValidInt();

            try
            {
                Console.WriteLine(a + " / " + b + " = " + (a / b));
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Unable to divide " + a + " with 0");
            }
        }

        private static int GetValidInt()
        {
            int n;

            Console.Write("Enter a number: ");

            while (true)
            {
                try
                {
                    n = int.Parse(Console.ReadLine());
                    return n;
                }

                catch (ArgumentNullException)
                {
                    Console.WriteLine("ArgumentNullException");
                }
                catch (FormatException)
                {
                    Console.Write("Please enter a valid number: ");
                }

            }
        }

        private static void RunExerciseTwentySix()
        {

            string desktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string fileName = "test.txt";
            string path = Path.Combine(desktop, fileName);
            FileStream stream;

            Console.WriteLine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
            Console.WriteLine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
            Console.WriteLine(Environment.GetFolderPath(Environment.SpecialFolder.Cookies));
            Console.WriteLine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86));
            Console.WriteLine(Directory.GetCurrentDirectory());

            if (!File.Exists(path))
            {
                stream = File.Create(path);
                stream.Close();

                Console.WriteLine("File has been created");
            }
            else
            {
                Console.WriteLine("File already exists");
            }

        }

        private static void RunExerciseTwentySeven()
        {
            using (StreamReader reader = new StreamReader("TestFile.txt"))
            {
                string name;

                while ((name = reader.ReadLine()) != null)
                {
                    Console.WriteLine(name);
                }
            }
        }

        private static void RunExerciseTwentyEight()
        {
            string[] arrayOne = { "Rose", "Gudrun", "Olof", "Annika", "Knut" };
            string[] arrayTwo = { "Bob", "Ross", "Parker", "Tomte", "Tommy" };

            if (File.Exists(@"C:\Users\lexicon\Desktop\test.txt"))
            {

                using (StreamWriter writer = new StreamWriter(@"C:\Users\lexicon\Desktop\test.txt"))
                {
                    for (int i = 0; i < arrayOne.Length; i++)
                    {
                        writer.WriteLine(arrayOne[i]);
                    }
                }
                // Append line to the file.
                using (StreamWriter writer = new StreamWriter(@"C:\Users\lexicon\Desktop\test.txt", true))
                {
                    for (int i = 0; i < arrayTwo.Length; i++)
                    {
                        writer.WriteLine(arrayTwo[i]);
                    }
                }

                Console.WriteLine("Names have been added to the file");
            }
            else
            {
                Console.WriteLine("The file does not exist");
            }
        }
    }
}
